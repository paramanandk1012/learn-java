package com.mindtree.demo.service;

import java.util.List;
import java.util.Optional;

import com.mindtree.demo.model.Course;

public interface CourseService {

	Course saveCourse(Course courseService);

	void deleteCourse(int courseId);

	List<Course> findAllCourse();

	Optional<Course> findByCourseId(int courseId);

}
