package com.mindtree.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.demo.model.Course;
import com.mindtree.demo.repo.CourseRepository;
import com.mindtree.demo.service.CourseService;

@Service
public class CourseSeviceImpl implements CourseService {

	@Autowired
	private CourseRepository courseRepository;

	@Override
	public Course saveCourse(Course courseService) {
		return courseRepository.save(courseService);

	}

	@Override
	public void deleteCourse(int courseId) {
		courseRepository.deleteById(courseId);

	}

	@Override
	public List<Course> findAllCourse() {
		
		return courseRepository.findAll();
	}

	@Override
	public Optional<Course> findByCourseId(int courseId) {
		
		return courseRepository.findById(courseId);
	}

}
