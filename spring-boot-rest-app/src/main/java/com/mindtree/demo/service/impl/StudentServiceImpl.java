package com.mindtree.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.demo.model.Student;
import com.mindtree.demo.repo.StudentRepository;
import com.mindtree.demo.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public Student saveStudent(Student studentService) {

		return studentRepository.save(studentService);
	}

	@Override
	public void deleteStudent(int sid) {

		studentRepository.deleteById(sid);

	}

	@Override
	public List<Student> findAllStudent() {

		return studentRepository.findAll();
	}

	@Override
	public Optional<Student> findByStudentId(int sid) {

		return studentRepository.findById(sid);
	}

}
