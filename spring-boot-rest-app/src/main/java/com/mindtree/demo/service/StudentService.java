package com.mindtree.demo.service;

import java.util.List;
import java.util.Optional;

import com.mindtree.demo.model.Student;

public interface StudentService {

	Student saveStudent(Student studentService);

	void deleteStudent(int sid);

	List<Student> findAllStudent();

	Optional<Student> findByStudentId(int sid);

}
