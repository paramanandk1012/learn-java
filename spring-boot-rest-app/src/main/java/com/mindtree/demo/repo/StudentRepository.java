package com.mindtree.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.demo.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>{
	
	

}
