package com.mindtree.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.demo.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

}
