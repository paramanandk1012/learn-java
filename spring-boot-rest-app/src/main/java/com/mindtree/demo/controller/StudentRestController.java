package com.mindtree.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.demo.model.Student;
import com.mindtree.demo.service.StudentService;

@RestController
@RequestMapping("/api")

public class StudentRestController {

	@Autowired
	private StudentService studentService;

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentRestController.class);

	@PostMapping("/save")
	public ResponseEntity<Student> createStudent(@Valid @RequestBody Student student) {

		Student st=studentService.saveStudent(student);
		return ResponseEntity.ok(st);
	}

	@GetMapping("/students")
	public ResponseEntity<List<Student>> findAllStudent() {

		return ResponseEntity.ok(studentService.findAllStudent());

	}

	@GetMapping("/{sid}")
	public ResponseEntity<Student> findByStudentId(@PathVariable Integer sid) {

		Optional<Student> student = studentService.findByStudentId(sid);
		if (!student.isPresent()) {
			LOGGER.error("Id " + sid + " is not existed");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(student.get());
	}

	@PutMapping("/{sid}")
	public ResponseEntity<Student> updateStudent(@PathVariable Integer sid, @Valid @RequestBody Student student) {
		if (!studentService.findByStudentId(sid).isPresent()) {
			LOGGER.error("Id " + sid + " is not existed");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(studentService.saveStudent(student));
	}

	@DeleteMapping("/{sid}")
	public ResponseEntity delete(@PathVariable Integer sid) {
		if (!studentService.findByStudentId(sid).isPresent()) {
			LOGGER.error("Id " + sid + " is not existed");
			ResponseEntity.badRequest().build();
		}

		studentService.deleteStudent(sid);

		return ResponseEntity.ok().build();
	}

}
