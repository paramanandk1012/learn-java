package com.mindtree.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.demo.model.Course;
import com.mindtree.demo.service.CourseService;

@RestController
@RequestMapping("/api/course")
public class CousreRestController {

	@Autowired
	private CourseService courseService;

	private static final Logger LOGGER = LoggerFactory.getLogger(CousreRestController.class);

	@PostMapping("/save")
	public ResponseEntity<Course> createCourse(@Valid @RequestBody Course course) {

		Course c = courseService.saveCourse(course);

		return ResponseEntity.ok(c);
	}

	@GetMapping("/cousres")
	public ResponseEntity<List<Course>> getAllCourse() {

		return ResponseEntity.ok(courseService.findAllCourse());

	}

	@GetMapping("/{cid}")
	public ResponseEntity<Course> findByCourseId(@PathVariable Integer cid) {

		Optional<Course> course = courseService.findByCourseId(cid);
		if (!course.isPresent()) {
			LOGGER.error("Id " + cid + " is not existed");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(course.get());
	}

	@PutMapping("/{cid}")
	public ResponseEntity<Course> updateCourse(@PathVariable Integer cid, @Valid @RequestBody Course course) {
		if (!courseService.findByCourseId(cid).isPresent()) {
			LOGGER.error("Id " + cid + " is not existed");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(courseService.saveCourse(course));
	}

	@DeleteMapping("/{sid}")
	public ResponseEntity delete(@PathVariable Integer cid) {
		if (!courseService.findByCourseId(cid).isPresent()) {
			LOGGER.error("Id " + cid + " is not existed");
			ResponseEntity.badRequest().build();
		}

		courseService.deleteCourse(cid);

		return ResponseEntity.ok().build();
	}

}
